#ifndef COUPLINGMAP_H
#define COUPLINGMAP_H
#include "utils/util.hpp"

#include <algorithm>
#include <bitset>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <functional>
#include <istream>
#include <ostream>
#include <regex>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <utility>
#include <variant>
#include <vector>

using QubitPairs = std::set<std::pair<unsigned short, unsigned short>>;

class CouplingMap {
public:
    void loadCouplingMap(const std::string& file);
    void loadCouplingMap(std::istream&& is);
    void loadCouplingMapDirect(const std::string& cm);
    void loadCouplingMapLinear(const std::string& cm);
    void loadCouplingMapCircular(const std::string& cm);
    void loadCouplingMapFromFidelityFile(std::istream&& is);

    void loadFidelity(const std::string& file);
    void loadFidelity(std::istream&& is);
    void loadFidelityRandom(const std::string& string);

    std::vector<std::set<unsigned short>> getAllConnectedSubsets(unsigned short nQubits);
    std::vector<QubitPairs>               getReducedCouplingMaps(unsigned short nQubits);
    QubitPairs                            getReducedCouplingMap(const std::set<unsigned short>& qubitChoice);
    std::vector<QubitPairs>               getHighestFidelityCouplingMap(unsigned short nQubits);
    static std::vector<unsigned short>    getQubitMap(const QubitPairs& couplingMap);

    [[nodiscard]] bool fidelitySet() const { return fidelityArchitectureName != "none"; }
    [[nodiscard]] bool archSet() const { return architectureName != "none"; }

    [[nodiscard]] std::string                      getArchitectureName() const { return architectureName; }
    [[nodiscard]] std::string                      getFidelityArchitectureName() const { return fidelityArchitectureName; }
    [[nodiscard]] unsigned int                     getArchitectureSize() const { return architectureSize; }
    [[nodiscard]] QubitPairs                       getCouplingMap() const { return couplingMap; }
    [[nodiscard]] std::vector<double>              getSingleFidelity() const { return singleFidelity; }
    [[nodiscard]] std::vector<std::vector<double>> getDoubleFidelity() const { return doubleFidelity; }
    [[nodiscard]] std::vector<double>              getLogSingleFidelity() const { return logSingleFidelity; }
    [[nodiscard]] std::vector<std::vector<double>> getLogDoubleFidelity() const { return logDoubleFidelity; }
    [[nodiscard]] std::vector<int>                 getSingleBucketFidelity() const { return singleBucketFidelity; }
    [[nodiscard]] std::vector<std::vector<int>>    getDoubleBucketFidelity() const { return doubleBucketFidelity; }

protected:
    std::string                      architectureName         = "none";
    std::string                      fidelityArchitectureName = "none";
    unsigned int                     architectureSize         = 0U;
    QubitPairs                       couplingMap{};
    std::set<unsigned short>         qubits{};
    std::vector<double>              singleFidelity{};
    std::vector<std::vector<double>> doubleFidelity{};
    std::vector<double>              logSingleFidelity{};
    std::vector<std::vector<double>> logDoubleFidelity{};
    std::vector<int>                 singleBucketFidelity{};
    std::vector<std::vector<int>>    doubleBucketFidelity{};

    void generateFidelities();
};

#endif // COUPLINGMAP_H
