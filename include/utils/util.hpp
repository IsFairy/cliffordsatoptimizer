//
// Created by Sarah on 12.07.2021.
//

#ifndef CLIFFORDSATOPT_UTIL_H
#define CLIFFORDSATOPT_UTIL_H

#include "LogicTerm/Logic.hpp"
#include "QuantumComputation.hpp"
#include "utils/logging.hpp"

#include <cmath>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <ostream>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace util {

    template<typename... Args>
    inline std::string string_format(const std::string& format, Args... args) {
        int size_s = std::snprintf(nullptr, 0, format.c_str(), args...) +
                     1; // Extra space for '\0'
        if (size_s <= 0) {
            error("Error during formatting.");
        }
        auto size = static_cast<std::size_t>(size_s);
        auto buf  = std::make_unique<char[]>(size);
        std::snprintf(buf.get(), size, format.c_str(), args...);
        return {buf.get(), buf.get() + size - 1}; // We don't want the '\0' inside
    }

    inline void pretty(const std::vector<std::vector<short>>& tableau,
                       std::ostream&                          os = std::cout) {
        if (tableau.empty()) {
            debug("Empty tableau");
            return;
        }
        for (std::size_t i = 0; i < tableau.back().size(); ++i) {
            os << i << "\t";
        }
        os << std::endl;
        auto i = 1;
        for (const auto& row: tableau) {
            if (row.size() != tableau.back().size()) {
                fatal("Tableau is not rectangular");
            }
            os << i++ << "\t";
            for (const auto& s: row)
                os << s << '\t';
            os << std::endl;
        }
    }

    inline std::string pretty_s(const std::vector<std::vector<short>>& tableau) {
        std::stringstream ss;
        pretty(tableau, ss);
        return ss.str();
    }

    inline std::vector<std::set<unsigned short>>
    subsets(const std::set<unsigned short>& input, int k) {
        std::size_t n = input.size();

        std::vector<std::set<unsigned short>> result;

        std::size_t i = (1U << k) - 1U;

        while (!(i >> n)) {
            std::set<unsigned short> v{};
            auto                     it = input.begin();

            for (std::size_t j = 0U; j < n; j++) {
                if (i & (1U << j)) {
                    v.emplace(*it);
                }
                std::advance(it, 1);
            }

            result.push_back(v);

            i = (i + (i & (-i))) | (((i ^ (i + (i & (-i)))) >> 2) / (i & (-i)));
        }

        return result;
    }

    inline void
    isFullyConnected(const unsigned short                         node,
                     const std::vector<std::set<unsigned short>>& connections,
                     std::vector<bool>&                           visited) {
        if (visited.at(node)) {
            return;
        }
        visited[node] = true;

        if (connections.at(node).empty()) {
            return;
        }

        for (const auto child: connections.at(node)) {
            isFullyConnected(child, connections, visited);
        }
    }

    inline std::set<std::pair<unsigned short, unsigned short>>
    getFullyConnectedMap(unsigned short nQubits) {
        std::set<std::pair<unsigned short, unsigned short>> result{};
        for (int q = 0; q < nQubits; ++q) {
            for (int p = q + 1; p < nQubits; ++p) {
                result.emplace(q, p);
                result.emplace(p, q);
            }
        }
        return result;
    }

    inline bool
    isFullyConnected(const std::set<std::pair<unsigned short, unsigned short>>& cm,
                     int nQubits, const std::set<unsigned short>& qubitChoice) {
        std::vector<std::set<unsigned short>> connections;
        std::vector<int>                      d;
        std::vector<bool>                     visited;
        connections.resize(nQubits);
        for (const auto& edge: cm) {
            if ((qubitChoice.count(edge.first) && qubitChoice.count(edge.second)) ||
                (qubitChoice.count(edge.second) && qubitChoice.count(edge.first))) {
                connections.at(edge.first).insert(edge.second);
                connections.at(edge.second).insert(edge.first);
            }
        }
        for (const auto q: qubitChoice) {
            visited.clear();
            visited.resize(nQubits, false);
            isFullyConnected(q, connections, visited);
            for (const auto p: qubitChoice) {
                if (!visited.at(p)) {
                    return false;
                }
            }
        }
        return true;
    }

    inline double
    getFidelity(const std::set<std::pair<unsigned short, unsigned short>>& cm,
                const std::set<unsigned short>&                            qubitChoice,
                const std::vector<double>&                                 singleFidelity,
                const std::vector<std::vector<double>>&                    doubleFidelity) {
        if (singleFidelity.empty() || doubleFidelity.empty()) {
            return 0.0;
        }
        double                                              result = 1.0;
        std::set<std::pair<unsigned short, unsigned short>> qubitPairs;
        for (const auto& edge: cm) {
            if (qubitChoice.find(edge.first) != qubitChoice.end() &&
                qubitChoice.find(edge.second) != qubitChoice.end()) {
                qubitPairs.emplace(edge.first, edge.second);
            }
        }
        for (const auto& edge: qubitPairs) {
            result *= doubleFidelity.at(edge.first).at(edge.second);
        }
        for (const auto& q: qubitChoice) {
            result *= singleFidelity.at(q);
        }
        return result;
    }

    inline bool checkEquality(const std::vector<std::vector<short>>& tableau1,
                              const std::vector<std::vector<short>>& tableau2,
                              int                                    nqubits) {
        if (tableau1.size() != tableau2.size()) {
            return false;
        }
        for (int i = 0; i < nqubits; ++i) {
            const auto& row1 = tableau1[i];
            const auto& row2 = tableau2[i];
            if (row1.size() != row2.size()) {
                return false;
            }
            for (int j = 0; j < 2 * nqubits + 1; ++j) {
                const auto& col1 = row1[j];
                const auto& col2 = row2[j];
                if (col1 != col2) {
                    return false;
                }
            }
        }
        return true;
    }

    inline void
    printCouplingMap(const std::set<std::pair<unsigned short, unsigned short>>& cm,
                     std::ostream&                                              os) {
        os << "{";
        for (const auto& edge: cm) {
            os << "(" << edge.first << " " << edge.second << ") ";
        }
        os << "}" << std::endl;
    }

    inline std::string printCouplingMap(
            const std::set<std::pair<unsigned short, unsigned short>>& cm) {
        std::stringstream ss;
        printCouplingMap(cm, ss);
        return ss.str();
    }

    inline void
    printFidelities(const std::vector<double>&              singleFidelities,
                    const std::vector<std::vector<double>>& doubleFidelities,
                    std::ostream&                           os) {
        os << "Single fidelities: ";
        int i = 0;
        for (auto f: singleFidelities) {
            os << "Qubit " << i++ << " = " << f << " ";
        }
        os << std::endl;
        os << "Double fidelities: ";
        int j = 0;
        for (const auto& f: doubleFidelities) {
            i = 0;
            for (auto f2: f) {
                if (f2 > 0.0) {
                    os << "Edge "
                       << "(" << i << "," << j << ") = " << f2 << " ";
                }
                i++;
            }
            os << std::endl;
            j++;
        }
    }

    inline std::string
    printFidelities(const std::vector<double>&              singleFidelities,
                    const std::vector<std::vector<double>>& doubleFidelities) {
        std::stringstream ss;
        printFidelities(singleFidelities, doubleFidelities, ss);
        return ss.str();
    }

    inline std::string escapeChars(const std::string& s, const std::string& chars) {
        std::stringstream ss;
        for (auto c: s) {
            if (chars.find(c) != std::string::npos) {
                ss << "\\" << c;
            } else if (c == '\n') {
                ss << "\\n";
            } else if (c == '\t') {
                ss << "\\t";
            } else {
                ss << c;
            }
        }
        return ss.str();
    }

    /*
 * @brief generates a Bitvector of length n from a 2d tableau
 * @param tableau The tableau to print
 * @param nqubits The number of qubits in the circuit
 * @param column The column number of the tableau to use
 */
    inline unsigned long getBVFrom(const std::vector<std::vector<short>>& tableau,
                                   int nqubits, int column) {
        unsigned long result = 0UL;
        for (int j = 0; j < nqubits; ++j) {
            if (tableau[j][column] == 1) {
                result |= (1U << j);
            }
        }
        return result;
    }

    inline void populateTableauFrom(unsigned long bv, int nqubits,
                                    std::vector<std::vector<short>>& tableau,
                                    int                              column) {
        for (int j = 0; j < nqubits; ++j) {
            if ((bv & (1U << j)) != 0U) {
                tableau[j][column] = 1;
            }
        }
    }

    inline std::string printVector(const std::vector<unsigned short>& v) {
        std::stringstream ss;
        ss << "[";
        for (const auto& d: v) {
            ss << d << " ";
        }
        ss << "]";
        return ss.str();
    }

    inline std::vector<std::vector<short>> getDiagonalTableau(int nqubits) {
        std::vector<std::vector<short>> result{};
        result.resize(nqubits);
        for (auto i = 0; i < nqubits; i++) {
            result[i].resize(2U * nqubits + 1U);
            for (auto j = 0; j < 2 * nqubits; j++) {
                if (i == j - nqubits) {
                    result[i][j] = 1;
                } else {
                    result[i][j] = 0;
                }
            }
            result[i][2U * nqubits] = 0;
        }

        return result;
    }

    inline double tableauDistance(const std::vector<std::vector<short>>& tableau1,
                                  const std::vector<std::vector<short>>& tableau2,
                                  int                                    nqubits) {
        double result = 0.0;
        if (tableau1.size() != tableau2.size()) {
            result = std::numeric_limits<double>::max();
        } else {
            for (int i = 0; i < nqubits; ++i) {
                auto first  = std::find_if(tableau1[i].begin(), tableau1[i].end(),
                                           [](short x) { return x == 1; });
                auto last   = std::find_if(tableau1[i].rbegin(), tableau1[i].rend(),
                                           [](short x) { return x == 1; });
                auto first2 = std::find_if(tableau2[i].begin(), tableau2[i].end(),
                                           [](short x) { return x == 1; });
                auto last2  = std::find_if(tableau2[i].rbegin(), tableau2[i].rend(),
                                           [](short x) { return x == 1; });
                auto d1     = std::distance(tableau1[i].begin(), first);
                auto d2     = std::distance(tableau1[i].rbegin(), last);
                auto d3     = std::distance(tableau2[i].begin(), first2);
                auto d4     = std::distance(tableau2[i].rbegin(), last2);
                result += static_cast<double>(std::abs(d1 - d3)) / 2.0 + static_cast<double>(std::abs(d2 - d4)) / 2.0;
            }
        }
        // DEBUG() << "Tableau distance: " << result << std::endl;
        return result;
    }

    inline std::vector<std::vector<short>>
    embedTableau(const std::vector<std::vector<short>>& tableau, int nqubits) {
        std::vector<std::vector<short>> result{};
        auto                            diagonal = getDiagonalTableau(nqubits);
        std::vector<int>                indices{};
        auto                            m = tableau.size();

        for (unsigned long i = 0U; i < static_cast<unsigned long>(nqubits); i++) {
            indices.push_back(i < m ? 0 : 1);
        }
        do {
            std::vector<std::vector<short>> intermediate_result{};
            int                             i = 0;
            intermediate_result.resize(nqubits);
            for (auto k = 0; k < nqubits; k++) {
                intermediate_result[k].resize(2 * nqubits + 1);
                int n = 0;
                for (auto j = 0; j < 2 * nqubits; j++) {
                    DEBUG() << "i = " << i << " n = " << n << " j = " << j << " k = " << k
                            << std::endl;
                    if (indices[k] == 1 || (j < nqubits && indices[j] == 1) ||
                        (j >= nqubits && indices[j - nqubits] == 1)) {
                        intermediate_result[k][j] = diagonal[k][j];
                    } else {
                        intermediate_result[k][j] = tableau[i][n];
                        n++;
                    }
                }
                if (indices[k] == 1) {
                    intermediate_result[k][2 * nqubits] = 0;
                } else {
                    intermediate_result[k][2 * nqubits] = tableau[i][2 * nqubits];
                    i++;
                }
                // intermediate_result[2 * nqubits] = tableau[2 * nqubits];
            }
            if (tableauDistance(diagonal, intermediate_result, nqubits) <
                tableauDistance(diagonal, result, nqubits)) {
                result = intermediate_result;
            }
        } while (std::next_permutation(indices.begin(), indices.end()));
        return result;
    }

    inline void getGateQubits(std::unique_ptr<qc::Operation>& gate, std::set<signed char>& qubits) {
        switch (gate->getType()) {
            case qc::OpType::H:
            case qc::OpType::Sdag:
            case qc::OpType::Z:
            case qc::OpType::Y:
            case qc::OpType::S: {
                if (gate->isControlled()) {
                    util::fatal("Expected single-qubit gate");
                }
                const auto a = gate->getTargets().at(0U);
                qubits.insert(a);
            } break;
            case qc::OpType::X: {
                if (gate->getNcontrols() != 1U) {
                    const auto a = gate->getTargets().at(0U);
                    qubits.insert(a);
                } else {
                    const auto a = (*gate->getControls().begin()).qubit;
                    const auto b = gate->getTargets().at(0);
                    qubits.insert(a);
                    qubits.insert(b);
                }
            } break;

            default:
                util::fatal("Unsupported gate encountered: " + std::to_string(gate->getType()));
                break;
        }
    }

} // namespace util

#endif // CLIFFORDSATOPT_UTIL_H
