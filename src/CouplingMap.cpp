#include "CouplingMap.hpp"

#include "utils/csv_util.hpp"
#include "utils/util.hpp"

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <regex>
#include <utility>
#include <vector>

void CouplingMap::loadCouplingMap(const std::string& filename) {
    std::string file = filename;
    file.erase(std::remove(file.begin(), file.end(), '\"'), file.end());
    file.erase(std::remove(file.begin(), file.end(), '\''), file.end());
    if (file.rfind("direct:", 0U) == 0U) {
        loadCouplingMapDirect(file);
    } else if (file.rfind("linear:", 0U) == 0U) {
        loadCouplingMapLinear(file);
    } else if (file.rfind("circular:", 0U) == 0U) {
        loadCouplingMapCircular(file);
    } else {
        size_t slash     = file.find_last_of('/');
        size_t dot       = file.find_last_of('.');
        architectureName = file.substr(slash + 1U, dot - slash - 1U);
        auto ifs         = std::ifstream(file);
        if (ifs.good()) {
            loadCouplingMap(std::move(ifs));
        } else {
            util::fatal("Error opening coupling map file.");
        }
    }
    DEBUG() << "Coupling Map (" << architectureSize << " qubits): ";
    DEBUG() << util::printCouplingMap(couplingMap);
}

void CouplingMap::loadCouplingMap(std::istream&& is) {
    couplingMap.clear();
    qubits.clear();
    std::string line;

    std::regex  r_nqubits = std::regex("([0-9]+)");
    std::regex  r_edge    = std::regex("([0-9]+) ([0-9]+)");
    std::smatch m;

    // get number of qubits
    if (std::getline(is, line)) {
        if (std::regex_match(line, m, r_nqubits)) {
            architectureSize = static_cast<unsigned short>(std::stoul(m.str(1)));
        } else {
            util::fatal("No qubit count found in coupling map file.");
        }
    } else {
        util::fatal("Error reading coupling map file.");
    }
    // load edges
    while (std::getline(is, line)) {
        if (std::regex_match(line, m, r_edge)) {
            auto v1 = static_cast<unsigned short>(std::stoul(m.str(1U)));
            auto v2 = static_cast<unsigned short>(std::stoul(m.str(2U)));
            couplingMap.emplace(v1, v2);
            qubits.insert(v1);
            qubits.insert(v2);
        } else {
            util::fatal("No qubit count found in coupling map file.");
        }
    }
    if (qubits.size() != architectureSize) {
        util::fatal("Size does not match with architecture size.");
    }
}
void CouplingMap::loadCouplingMapDirect(const std::string& cm) {
    couplingMap.clear();
    qubits.clear();
    std::string line = cm;

    std::regex  r_edge = std::regex("(([0-9]+) ([0-9]+))");
    std::smatch m;
    while (std::regex_search(line, m, r_edge)) {
        auto v1 = static_cast<unsigned short>(std::stoul(m.str(2)));
        auto v2 = static_cast<unsigned short>(std::stoul(m.str(3)));
        couplingMap.emplace(v1, v2);
        qubits.insert(v1);
        qubits.insert(v2);
        line = m.suffix();
    }
    architectureSize = qubits.size();
    architectureName = "Direct";
}

void CouplingMap::loadCouplingMapLinear(const std::string& cm) {
    couplingMap.clear();
    qubits.clear();

    std::regex  r_edge = std::regex("([0-9]+)");
    std::smatch m;
    if (std::regex_search(cm, m, r_edge)) {
        auto size = static_cast<unsigned short>(std::stoul(m.str(1U)));
        for (auto i = 1U; i < size; ++i) {
            couplingMap.emplace(i - 1U, i);
            couplingMap.emplace(i, i - 1U);
            qubits.insert(i);
        }
        qubits.insert(0U);
    }
    architectureSize = qubits.size();
    architectureName = "Linear";
}

void CouplingMap::loadCouplingMapCircular(const std::string& cm) {
    couplingMap.clear();
    qubits.clear();

    std::regex  r_edge = std::regex("([0-9]+)");
    std::smatch m;
    if (std::regex_search(cm, m, r_edge)) {
        auto size = static_cast<unsigned short>(std::stoul(m.str(1U)));
        for (auto i = 1U; i <= size; ++i) {
            couplingMap.emplace(i - 1U, i);
            couplingMap.emplace(i, i - 1U);
            qubits.insert(i);
        }
        couplingMap.emplace(size, 0U);
        couplingMap.emplace(0U, size);
        qubits.insert(0U);
    }
    architectureSize = qubits.size();
    architectureName = "Circular";
}

void CouplingMap::loadCouplingMapFromFidelityFile(std::istream&& is) {
    couplingMap.clear();
    qubits.clear();
    std::string line;
    architectureName   = fidelityArchitectureName;
    std::regex  r_cnot = std::regex(R"(((\d+?)\_(\d+?):))");
    std::smatch m;
    if (!std::getline(is, line)) {
        util::fatal("Error reading fidelity file.");
    }
    while (std::getline(is, line)) {
        std::stringstream ss(line);
        while (std::regex_search(line, m, r_cnot)) {
            auto q1 = static_cast<unsigned short>(std::stoul(m.str(2)));
            auto q2 = static_cast<unsigned short>(std::stoul(m.str(3)));
            couplingMap.emplace(q1, q2);
            qubits.insert(q1);
            qubits.insert(q2);
            line = m.suffix();
        }
    }
    architectureSize = qubits.size();
}

void CouplingMap::loadFidelity(const std::string& file) {
    std::string fname = file;
    fname.erase(std::remove(fname.begin(), fname.end(), '\"'), fname.end());
    fname.erase(std::remove(fname.begin(), fname.end(), '\''), fname.end());
    if (file.rfind("random:", 0U) == 0U) {
        loadFidelityRandom(file);
    } else {
        size_t slash             = fname.find_last_of('/');
        size_t dot               = fname.find_last_of('.');
        fidelityArchitectureName = fname.substr(slash + 1U, dot - slash - 1U);
        auto ifs                 = std::ifstream(fname);
        if (ifs.good()) {
            if (architectureSize == 0U) {
                auto ifs2 = std::ifstream(fname);
                loadCouplingMapFromFidelityFile(std::move(ifs2));
            }
            loadFidelity(std::move(ifs));
        } else {
            util::fatal("Error opening fidelity file.");
        }
    }

    DEBUG() << util::printFidelities(singleFidelity, doubleFidelity);
    generateFidelities();
}
void CouplingMap::loadFidelity(std::istream&& is) {
    singleFidelity.clear();
    doubleFidelity.clear();
    for (unsigned int i = 0U; i < architectureSize; ++i) {
        doubleFidelity.emplace_back();
        doubleFidelity[i].resize(architectureSize);
    }
    std::string line;

    std::string word;
    int         singlepos = 0;
    int         doublepos = 0;
    std::regex  r_dfidelity =
            std::regex(R"(((\d+).?(\d+):\W*?(\d+\.\d+e?-?\d+)))");
    std::smatch m;

    if (std::getline(is, line)) {
        std::stringstream ss(line);
        int               pos = 0;
        while (getline(ss, word, ',')) {
            if (word.find("Single-qubit") != std::string::npos) {
                singlepos = pos;
            }
            if (word.find("CNOT error") != std::string::npos) {
                doublepos = pos;
            }
            pos++;
        }
    } else {
        util::fatal("Error reading fidelity file.");
    }
    // load edges
    while (std::getline(is, line)) {
        std::stringstream ss(line);
        int               pos = 0;
        for (const auto& w: CSV::parse_line(line, {'\"'}, {'\\'})) {
            if (pos == singlepos)
                singleFidelity.emplace_back(1. - std::stod(w));
            if (pos == doublepos) {
                std::string s = w;
                while (std::regex_search(s, m, r_dfidelity)) {
                    auto a               = static_cast<unsigned int>(std::stoul(m.str(2U)));
                    auto b               = static_cast<unsigned int>(std::stoul(m.str(3U)));
                    doubleFidelity[a][b] = 1. - std::stod(m.str(4U));
                    s                    = m.suffix().str();
                }
            }
            pos++;
        }
    }
}

void CouplingMap::loadFidelityRandom(const std::string& string) {
    if (architectureName == "none") {
        util::fatal("CouplingMap: Architecture not set.");
    }

    std::regex  r_seed = std::regex("([0-9]+)");
    std::smatch m;
    if (std::regex_search(string, m, r_seed)) {
        srand(std::stoul(m.str(1U)));
        for (unsigned int i = 0U; i < architectureSize; ++i) {
            doubleFidelity.emplace_back();
            for (unsigned int j = 0U; j < architectureSize; ++j) {
                doubleFidelity.back().emplace_back(1. - (static_cast<double>(rand() % 10) / 1000));
            }
        }
        for (unsigned int i = 0U; i < architectureSize; ++i) {
            singleFidelity.emplace_back(1. - (static_cast<double>(rand() % 10) / 1000));
        }
        qubits.insert(0U);
    }
    architectureSize = qubits.size();
    architectureName = "Linear";
}

std::vector<std::set<unsigned short>>
CouplingMap::getAllConnectedSubsets(unsigned short nQubits) {
    std::vector<std::set<unsigned short>> result{};
    if (architectureName == "none" || architectureSize == nQubits) {
        result.emplace_back();
        for (unsigned short i = 0U; i < nQubits; ++i) {
            result.back().emplace(i);
        }
    } else if (architectureSize < nQubits) {
        util::fatal("Architecture to small for qubit count");
    } else {
        for (const auto& subset: util::subsets(qubits, nQubits)) {
            if (util::isFullyConnected(couplingMap, static_cast<int>(architectureSize), subset)) {
                result.emplace_back(subset);
            }
        }
    }
    return result;
}

std::vector<std::set<std::pair<unsigned short, unsigned short>>>
CouplingMap::getReducedCouplingMaps(unsigned short nQubits) {
    std::vector<std::set<std::pair<unsigned short, unsigned short>>> result{};
    if (architectureName == "none") {
        result.emplace_back(util::getFullyConnectedMap(nQubits));
    } else {
        for (const auto& qubitChoice: getAllConnectedSubsets(nQubits)) {
            result.emplace_back(getReducedCouplingMap(qubitChoice));
        }
    }
    return result;
}

std::set<std::pair<unsigned short, unsigned short>>
CouplingMap::getReducedCouplingMap(
        const std::set<unsigned short>& qubitChoice) {
    std::set<std::pair<unsigned short, unsigned short>> result{};
    if (architectureName == "none") {
        result = util::getFullyConnectedMap(qubitChoice.size());
    } else {
        for (const auto& [q0, q1]: couplingMap) {
            if (qubitChoice.find(q0) != qubitChoice.end() && qubitChoice.find(q1) != qubitChoice.end()) {
                result.emplace(q0, q1);
            }
        }
    }
    return result;
}

void CouplingMap::generateFidelities() {
    logDoubleFidelity.clear();
    logDoubleFidelity.reserve(doubleFidelity.size());
    for (const auto& f: doubleFidelity) {
        logDoubleFidelity.emplace_back();
        for (const auto& f2: f) {
            logDoubleFidelity.back().emplace_back(std::log(f2));
        }
    }
    logSingleFidelity.clear();
    logSingleFidelity.reserve(singleFidelity.size());
    for (const auto& f: singleFidelity) {
        logSingleFidelity.emplace_back(std::log(f));
    }
    // Calculate Buckets for the log fidelities, to map them to a uniform, but
    // sorted range of integers
    // simply multiplying does not work here
    singleBucketFidelity.clear();
    doubleBucketFidelity.clear();
    singleBucketFidelity.resize(architectureSize);
    doubleBucketFidelity.resize(architectureSize);
    for (unsigned int i = 0U; i < architectureSize; ++i) {
        doubleBucketFidelity[i].resize(architectureSize);
    }
    std::vector<double> allFidelities{logSingleFidelity.begin(), logSingleFidelity.end()};
    for (const auto& f: logDoubleFidelity) {
        allFidelities.insert(allFidelities.end(), f.begin(), f.end());
    }
    std::sort(allFidelities.begin(), allFidelities.end());
    allFidelities.erase(std::unique(allFidelities.begin(), allFidelities.end()), allFidelities.end());
    for (unsigned int i = 0U; i < allFidelities.size(); ++i) {
        const auto singleIt = std::find(logSingleFidelity.begin(), logSingleFidelity.end(), allFidelities[i]);
        if (singleIt != logSingleFidelity.end()) {
            singleBucketFidelity[std::abs(std::distance(singleIt, logSingleFidelity.begin()))] = static_cast<int>(i);
        }
        for (unsigned int j = 0U; j < logDoubleFidelity.size(); ++j) {
            auto doubleIt = std::find(logDoubleFidelity[j].begin(), logDoubleFidelity[j].end(), allFidelities[i]);
            if (doubleIt != logDoubleFidelity[i].end()) {
                auto d                     = std::abs(std::distance(doubleIt, logDoubleFidelity[j].begin()));
                doubleBucketFidelity[j][d] = static_cast<int>(i);
            }
        }
    }
}

std::vector<QubitPairs>
CouplingMap::getHighestFidelityCouplingMap(unsigned short nQubits) {
    std::vector<QubitPairs> result{};
    if (architectureName == "none" || architectureSize == nQubits ||
        fidelityArchitectureName == "none") {
        result.emplace_back(util::getFullyConnectedMap(nQubits));
    } else {
        double              bestFidelity{};
        std::vector<double> allFidelities{};
        auto                allConnectedSubsets = getAllConnectedSubsets(nQubits);
        result.reserve(allConnectedSubsets.size());
        allFidelities.reserve(allConnectedSubsets.size());
        for (const auto& qubitChoice: allConnectedSubsets) {
            auto map     = getReducedCouplingMap(qubitChoice);
            bestFidelity = util::getFidelity(map, qubitChoice, singleFidelity, doubleFidelity);
            if (allFidelities.empty()) {
                allFidelities.emplace_back(bestFidelity);
                result.emplace_back(map);
            } else {
                auto it = allFidelities.begin();
                while (it != allFidelities.end() && *it < bestFidelity) {
                    std::advance(it, 1);
                }
                const auto distance = std::abs(std::distance(allFidelities.begin(), it));
                allFidelities.emplace(allFidelities.begin() + distance, bestFidelity);
                result.emplace(result.begin() + distance, map);
            }
        }
    }
    return result;
}

std::vector<unsigned short>
CouplingMap::getQubitMap(const QubitPairs& cm) {
    std::set<unsigned short> result{};
    for (const auto& edge: cm) {
        result.emplace(edge.first);
        result.emplace(edge.second);
    }
    return {result.begin(), result.end()};
}
